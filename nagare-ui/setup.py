VERSION = '0.0.1'

from setuptools import setup, find_packages

setup(
    name='nagare-ui',
    version=VERSION,
    author='',
    author_email='herve.coatanhay@gmail.com',
    description='Collection of nagare widgets',
    long_description=open('README.rst', 'r').read(),
    keywords='nagare widget',
    url='https://bitbucket.org/Alzakath/nagare.contrib',
    packages=find_packages(),
    license='LICENSE.txt',
    zip_safe=False,
    install_requires=('nagare',),
    namespace_packages=('nagare', 'nagare.contrib', ),
)
