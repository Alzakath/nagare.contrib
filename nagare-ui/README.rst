=========
nagare-ui
=========

A collection of widgets for ``Nagare``.


Usage
=====

Calendar
--------
See test `Application <nagare/contrib/ui/tests/test_calendar.py#cl-50>`_

Menu
----
See test  `Application <nagare/contrib/ui/tests/test_menu.py#cl-58>`_

Modal
-----
See test `Application <nagare/contrib/ui/tests/test_modal.py#cl-65>`_

Paginator
---------
See test `Application <nagare/contrib/ui/tests/test_paginator.py#cl-69>`_

Test Application
----------------
A test application that shows all components in action is also available.
It can be run as follow:

.. code-block:: sh

    $ nagare-admin serve nagare-ui/nagare/contrib/ui/tests/test.conf


Requirements
============

* Python 2.6+ with nagare latest
* Stackless Python 2.6+ with Nagare >= 0.4.1


License
=======

BSD


Running Tests
=============

.. code-block:: sh

    $ hg clone https://bitbucket.org/Alzakath/nagare.contrib
    $ cd nagare-ui
    $ nosetests -v


Changelog
=========

Dev
    *

v0.0.1
    * Initial release