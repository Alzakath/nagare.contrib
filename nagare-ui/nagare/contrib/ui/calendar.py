# -*- coding:utf-8 -*-
"""A nagare component used for navigation."""
from __future__ import absolute_import
import calendar
import datetime
from nagare import var, presentation
from . import features


class Calendar(object):

    """

    """

    def __init__(self):
        self.selected = var.Var(None)
        today = datetime.datetime.now()
        self.selected((today.year, today.month, today.day))

    def select(self, year, month, day, comp):
        """
        Callbacks that fire click event in navigation.
        """
        if year <= 0:
            raise ValueError('yead must be a positive integer %d' %
                             year)

        if month <= 0 or month > 12:
            raise ValueError('month must be an integer between 1 and 12')

        firstday_weekday, ndays = calendar.monthrange(year, month)

        if day <= 0 or day > ndays:
            raise ValueError('day must be an integer between 1 and %s' % ndays)

        comp.answer((year, month, day))

    def is_today(self):
        today = datetime.datetime.now()

        def f(year, month, day):
            return (year, month, day) == (today.year, today.month, today.day)
        return f


@presentation.render_for(Calendar)
def render_calendar(self, h, comp, *args):
    """
    Render html for Calendar, component actions are links.

    Args:
        h: Nagare html renderer
        comp: Nagare Component to render
        *args: unused arguments needed for Nagare
    """
    year, month, day = self.selected()

    is_today = self.is_today()

    with h.div(class_='datepicker'):
        with h.table:
            h << h.tr(h.th(calendar.month_abbr[month], colspan=7))
            with h.tr:
                for d in xrange(7):
                    h << h.td(calendar.day_abbr[d])

            for week in calendar.monthcalendar(year, month):
                with h.tr:
                    for d in week:
                        cls = ['day']
                        if is_today(year, month, d):
                            cls.append('today')

                        if not d:
                            cls.append('disabled')
                        elif d == day:
                            cls.append('selected')

                        with h.td(class_=' '.join(cls)):
                            if d:
                                if features.has_partial:
                                    h << h.a(d).action(
                                        features.Partial(
                                            self.select, year, month, d, comp)
                                    )
                                else:
                                    h << h.a(d).action(
                                        lambda d=d: self.select(
                                            year, month, d, comp)
                                    )

    return h.root
