"""Module used to test available Nagare features """

try:
    from nagare.partial import Partial
    has_partial = True
except ImportError:
    has_partial = False

try:
    from nagare import continuation
    has_continuation = continuation.has_continuation
except ImportError:
    import stackless
    has_continuation = True
