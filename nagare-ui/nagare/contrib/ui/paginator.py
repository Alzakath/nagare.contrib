# -*- coding:utf-8 -*-
"""A nagare component used for list pagination."""
from math import ceil
from nagare import var, presentation
from nagare.i18n import _
from . import features


class PaginatorMenu(object):

    """
    This a base class to handle objects list pagination navigation.

    Attributes:
        page: current page index
        per_page: number of items per page
        total_count: total number of items
    """

    def __init__(self, page, per_page, total_count):
        """
        Initialize Menu navigation component.

        Args:
            page: page index
            per_page: number of items per page
            total_count: total number of pages
        """
        self.page = var.Var(page)
        self.per_page = per_page
        self.total_count = total_count

    @property
    def pages(self):
        """
        Number of pages.
        """
        return int(ceil(self.total_count / float(self.per_page)))

    @property
    def has_prev(self):
        """
        Tells if there are pages before current page.
        """
        return self.page() > 1

    @property
    def has_next(self):
        """
        Tells if there are pages after current page.
        """
        return self.page() < self.pages

    def display(self, i, comp):
        """
        Callbacks that fire click event in navigation.
        """
        if i < 1 or i > self.pages:
            raise ValueError('index must be between 1 and %d' % self.pages)

        comp.answer(i)

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=5, right_edge=2):
        """
        Compute the list of
        """
        last = 0
        for num in xrange(1, self.pages + 1):
            if self.is_in_range(num, left_edge, left_current, right_current,
                                right_edge):
                if last + 1 != num:
                    yield None
                yield num
                last = num

    def is_in_range(self, num, left_edge, left_current, right_current,
                    right_edge):
        """
        Test if a page number is in current pages list
        """
        return any((
            num <= left_edge,
            (num > self.page() - left_current - 1 and
             num < self.page() + right_current),
            num > self.pages - right_edge)
        )


@presentation.render_for(PaginatorMenu)
def render_pagination_menu(self, h, comp, *args):
    """
    Render html for PaginatorMenu navigation within pagination context,
    component actions are links. It uses styles from Twitter boostrap.

    Args:
        h: Nagare html renderer
        comp: Nagare Component to render
        *args: unused arguments needed for Nagare
    """

    with h.ul(class_="pagination"):
        for entry in self.iter_pages():
            if entry:
                if self.page() == entry:
                    li = h.li(class_='active')
                else:
                    li = h.li

                with li:
                    if features.has_partial:
                        h << h.a(entry, title=_('Page %d') % entry).action(
                            features.Partial(self.display, entry, comp)
                        )
                    else:
                        h << h.a(entry, title=_('Page %d') % entry).action(
                            lambda entry=entry: self.display(entry, comp)
                        )
            else:
                h << h.li(h.span('...'), class_='disabled')
    return h.root
