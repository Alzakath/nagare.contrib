# -*- coding:utf-8 -*-
"""A nagare component used for navigation."""
from nagare import var, presentation
from . import features


class Menu(object):

    """
    This a base class to handle navigation.

    Attributes:
        items: list of objects to handle
        selected: index of the selected object
    """

    def __init__(self, items, classes=(), with_link=False):
        """
        Initialize Menu navigation component.

        Args:
            items: a list of object
        """
        self.items = items
        self.selected = var.Var(None)
        self.classes = classes
        self.with_link = with_link

    def display(self, i, comp):
        """
        Callbacks that fire click event in navigation.
        """
        if i < 0 or i >= len(self.items):
            raise ValueError('index must be between 1 and %d' %
                             len(self.items))
        comp.answer(i)


@presentation.render_for(Menu)
def render(self, h, comp, *args):
    """
    Render default html for Menu navigation component. It uses styles
    from Twitter bootstrap.

    Args:
        h: Nagare html renderer
        comp: Nagare Component to render
        *args: unused arguments needed for Nagare
    """
    with h.ul(class_=' '.join(self.classes)):

        for index, entry in enumerate(self.items):
            if entry:
                if self.selected() == index:
                    li = h.li(class_='active')
                else:
                    li = h.li

                with li:
                    if features.has_partial:
                        h << h.a(entry, href=entry if self.with_link else '', title=entry).action(
                            features.Partial(self.display, index, comp)
                        )
                    else:
                        h << h.a(entry, href=entry if self.with_link else '', title=entry).action(
                            lambda index=index: self.display(index, comp)
                        )
            else:
                h << h.li(class_='divider')

    return h.root


@presentation.render_for(Menu, model='form')
def render_form(self, h, comp, *args):
    """
    Render html for Menu navigation component actions are submit buttons not
    links. It uses styles from Twitter boostrap. This html fragment as to be
    wrapped into an html form.

    Args:
        h: Nagare html renderer
        comp: Nagare Component to render
        *args: unused arguments needed for Nagare
    """
    with h.ul(class_=' '.join(self.classes)):
        for index, entry in enumerate(self.items):
            if entry:
                if self.selected() == index:
                    li = h.li(class_='active')
                else:
                    li = h.li

                with li:
                    if features.has_partial:
                        h << h.input(value=entry, title=entry,
                                     type='submit', class_='btn').action(
                            features.Partial(
                                self.display, index, comp)
                        )
                    else:
                        h << h.input(value=entry, title=entry,
                                     type='submit', class_='btn').action(
                            lambda index=index: self.display(
                                index, comp)
                        )
            else:
                h << h.li(class_='divider')

    return h.root
