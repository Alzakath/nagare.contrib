from nagare import callbacks
from nagare.namespaces.xhtml import Renderer


def create_renderer():
    callbacks_factory = getattr(callbacks, 'Callbacks', None)
    if callbacks_factory:
        return Renderer(callbacks=callbacks.Callbacks())
    else:
        return Renderer()
