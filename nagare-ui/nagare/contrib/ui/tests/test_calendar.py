# -*- coding:utf-8 -*-
import datetime
import unittest
from nagare import component, presentation
from nagare.contrib.ui import calendar
from .utils import create_renderer


class CalendarTest(unittest.TestCase):

    def setUp(self):
        self.calendar = component.Component(calendar.Calendar())
        self.calendar.on_answer(self.calendar().selected)

    def tearDown(self):
        self.calendar = None

    def test_display(self):
        """Select a date"""

        self.calendar().select(2014, 2, 28, self.calendar)

        self.assertEqual(self.calendar().selected(), (2014, 2, 28))

    def test_wrong_day(self):
        """Out of range day"""
        with self.assertRaises(ValueError):
            self.calendar().select(2014, 2, 29, self.calendar)

    def test_wrong_month(self):
        """Month bigger than 12"""
        with self.assertRaises(ValueError):
            self.calendar().select(2014, 13, 28, self.calendar)

    def test_zero_year(self):
        """Year Zero"""
        with self.assertRaises(ValueError):
            self.calendar().select(0, 2, 28, self.calendar)

    def test_render(self):
        h = create_renderer()
        s = self.calendar.render(h)
        self.assertEqual(len(s.xpath("//td[@class='day today']")), 0)
        self.assertEqual(len(s.xpath("//td[@class='day today selected']")), 1)
        self.assertEqual(s.attrib['class'], 'datepicker')


# serve-module tests


class App1:

    """
    Base app to test calendar.
    """

    def __init__(self):
        self.calendar = component.Component(
            calendar.Calendar()
        )
        self.calendar.on_answer(self.calendar().selected)
        now = datetime.datetime.now()
        self.calendar().select(now.year, now.month, now.day, self.calendar)


@presentation.render_for(App1)
def render_app1(self, h, comp, *args):
    h << self.calendar
    h << h.hr
    with h.div:
        h << h.span(' / '.join(str(i) for i in self.calendar().selected()))

    return h.root
