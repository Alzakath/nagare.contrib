# -*- coding:utf-8 -*-
from nagare import component, presentation
from nagare.contrib.ui import menu
from nagare.contrib.ui.tests import (test_menu, test_paginator, test_modal,
                                     test_calendar)


class App(object):

    def __init__(self):

        self.tabs = [('menu1', test_menu.App1),
                     ('menu2', test_menu.App2),
                     ('paginator', test_paginator.App1),
                     ('modal', test_modal.App1),
                     ('calendar', test_calendar.App1), ]

        self.content = component.Component(None)

        self.menu = component.Component(
            menu.Menu([tab[0] for tab in self.tabs]))
        self.menu.on_answer(self.select_tab)

        self.select_tab(0)

    def select_tab(self, index):
        self.content.becomes(self.tabs[index][1]())
        self.menu().selected(index)


@presentation.render_for(App)
def render(self, h, comp, *args):
    # load js and css from bootstrap
    h.head.css_url('http://netdna.bootstrapcdn.com/twitter-bootstrap/2.1.1/'
                   'css/bootstrap-combined.min.css')
    h.head.css_url('http://cdnjs.cloudflare.com/ajax/libs/'
                   'bootstrap-datepicker/1.3.0/css/datepicker.css')

    h.head.javascript_url('http://cdnjs.cloudflare.com/ajax/libs/jquery/'
                          '1.8.2/jquery.min.js')
    h.head.javascript_url('http://netdna.bootstrapcdn.com/twitter-bootstrap/'
                          '2.1.1/js/bootstrap.min.js')
    with h.div(class_='container'):
        with h.div(class_='row'):
            with h.div(class_='span3'):
                h << self.menu
            with h.div(class_='span9'):
                h << self.content.render(h.AsyncRenderer())

    return h.root
