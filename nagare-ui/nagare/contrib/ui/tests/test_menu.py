# -*- coding:utf-8 -*-
import unittest
from nagare import component, presentation
from nagare.contrib.ui import menu
from .utils import create_renderer


class MenuTest(unittest.TestCase):

    def setUp(self):
        self.menu = component.Component(menu.Menu(
            ['one', 'two', 'three', 'four', 'five'],
            classes=('nav', 'nav-list')))
        self.menu.on_answer(self.menu().selected)

    def tearDown(self):
        self.menu = None

    def test_display(self):
        """Display an element"""

        self.menu().display(3, self.menu)

        self.assertEqual(self.menu().items[self.menu().selected()], 'four')

    def test_display_toobig(self):
        """Item index too big"""
        with self.assertRaises(ValueError):
            self.menu().display(5, self.menu)

    def test_display_toosmall(self):
        """Item index too small"""
        with self.assertRaises(ValueError):
            self.menu().display(-1, self.menu)

    def test_render(self):
        h = create_renderer()
        self.menu().display(3, self.menu)
        s = self.menu.render(h)

        self.assertEqual(s.attrib['class'], 'nav nav-list')
        self.assertEqual(len(s.findall(".//a")), 5)
        self.assertEqual(len(s.findall(".//li[@class='active']")), 1)
        self.assertEqual(len(s.findall(".//input")), 0)

    def test_render_form(self):
        h = create_renderer()
        self.menu().display(3, self.menu)
        s = self.menu.render(h, model='form')

        self.assertEqual(s.attrib['class'], 'nav nav-list')
        self.assertEqual(len(s.findall(".//a")), 0)
        self.assertEqual(len(s.findall(".//li[@class='active']")), 1)
        self.assertEqual(len(s.findall(".//input")), 5)

# serve-module tests


class App1:

    """
    Base app to test menu. menu actions are links
    """

    def __init__(self):
        self.menu = component.Component(
            menu.Menu(
                ['one', 'two', 'three', 'four', 'five'],
                classes=('nav', 'nav-list')
            )
        )
        self.menu.on_answer(self.menu().selected)
        self.menu().display(3, self.menu)


@presentation.render_for(App1)
def render_app1(self, h, comp, *args):
    h << self.menu
    h << h.hr
    with h.div:
        h << self.menu().items[self.menu().selected()]

    return h.root


class App2(App1):

    """
    Base app to test menu. menu actions are submit
    """


@presentation.render_for(App2)
def render_app2(self, h, comp, *args):
    with h.form:
        h << self.menu.render(h, model='form')
        h << h.hr
        with h.div:
            h << self.menu().items[self.menu().selected()]

    return h.root
