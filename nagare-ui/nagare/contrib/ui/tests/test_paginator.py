# -*- coding:utf-8 -*-
import unittest
from nagare import component, presentation
from nagare.contrib.ui import paginator
from .utils import create_renderer


class MenuTest(unittest.TestCase):

    def setUp(self):
        self.paginator = component.Component(paginator.PaginatorMenu(0, 15,
                                                                     200))
        self.paginator.on_answer(self.paginator().page)

    def tearDown(self):
        self.menu = None

    def test_display(self):
        """Select a page"""

        self.paginator().display(3, self.paginator)

        self.assertEqual(self.paginator().page(), 3)
        self.assertEqual(self.paginator().pages, 14)
        self.assertEqual(self.paginator().has_prev, True)
        self.assertEqual(self.paginator().has_next, True)

    def test_display_prev(self):
        """Select first page"""

        self.paginator().display(1, self.paginator)

        self.assertEqual(self.paginator().page(), 1)
        self.assertEqual(self.paginator().pages, 14)
        self.assertEqual(self.paginator().has_prev, False)
        self.assertEqual(self.paginator().has_next, True)

    def test_display_next(self):
        """Select last page"""

        self.paginator().display(14, self.paginator)

        self.assertEqual(self.paginator().page(), 14)
        self.assertEqual(self.paginator().has_prev, True)
        self.assertEqual(self.paginator().has_next, False)

    def test_display_toobig(self):
        """Page index too big"""
        with self.assertRaises(ValueError):
            self.paginator().display(15, self.paginator)

    def test_display_toosmall(self):
        """Page index too small"""
        with self.assertRaises(ValueError):
            self.paginator().display(-1, self.paginator)

    def test_render(self):
        h = create_renderer()
        self.paginator().display(3, self.paginator)
        s = self.paginator.render(h)

        self.assertEqual(s.attrib['class'], 'pagination')
        self.assertEqual(len(s.findall(".//a")), 9)
        self.assertEqual(len(s.findall(".//li[@class='active']")), 1)


# serve-module tests

class App1:

    """
    Base app to test paginator.
    """

    def __init__(self):
        self.paginator = component.Component(paginator.PaginatorMenu(0, 15,
                                                                     200))
        self.paginator.on_answer(self.paginator().page)


@presentation.render_for(App1)
def render_form(self, h, comp, *args):
    h << self.paginator
    h << h.hr
    with h.div:
        h << self.paginator().page

    return h.root
