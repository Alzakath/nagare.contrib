#-*- coding:utf-8 -*-
import unittest
from nagare import component, presentation
from nagare.contrib.ui import modal, features
from .utils import create_renderer


class Dummy(object):
    pass


@presentation.render_for(Dummy, model='dummy')
def render_dummy(self, h, comp, *args):
    return 'dummy'


@presentation.render_for(Dummy, model='modal')
def render_dummy_modal(self, h, comp, *args):
    return 'dummy-modal'


@presentation.render_for(Dummy, model='modal-footer')
def render_dummy_modal_footer(self, h, comp, *args):
    return 'dummy-modal-footer'


class ModalTest(unittest.TestCase):

    def setUp(self):
        self._dummy = Dummy()
        self.modal = component.Component(
            modal.Modal(component.Component(self._dummy, model='dummy'),
                        'modal',
                        'modal title'))

    def tearDown(self):
        self._dummy = None
        self.modal = None

    def test_init(self):
        """Test Modal initialization"""

        self.assertEqual(self.modal().title, 'modal title')
        self.assertEqual(self.modal().comp.model, 'dummy')
        self.assertEqual(self.modal().comp(), self._dummy)
        self.assertEqual(self.modal().body(), self._dummy)
        self.assertEqual(self.modal().footer(), self._dummy)

    def test_render(self):
        h = create_renderer()
        s = self.modal.render(h)

        self.assertEqual(s.attrib['class'], 'modal-wrap')
        self.assertEqual(s.text, 'dummy')
        self.assertEqual(len(s.findall(".//div[@class='modal-footer']")), 1)
        self.assertEqual(
            s.findall(".//div[@class='modal-footer']")[0].text,
            'dummy-modal-footer')
        self.assertEqual(len(s.findall(".//div[@class='modal-body']")), 1)
        self.assertEqual(
            s.findall(".//div[@class='modal-body']")[0].text, 'dummy-modal')


# serve-module tests
class App1(object):

    """
    Base app to test modal.
    """

    def __init__(self):
        self.modal = modal.Modal(component.Component(self),
                                 'modal',
                                 'modal title')
        self.counter = 0
        self.message = ""

    def incr(self):
        self.counter += 1

    def commit(self, comp):
        if self.counter < 5:
            comp.answer(self.counter)
        else:
            self.message = "counter is bigger than 5"


if features.has_continuation:
    @presentation.render_for(App1)
    def render_app1(self, h, comp, *args):
        if features.has_partial:
            h << h.a(
                'open modal',
                role='button',
                class_='btn btn-primary btn-large'
            ).action(features.Partial(comp.call, self.modal))
        else:
            h << h.a(
                'open modal',
                role='button',
                class_='btn btn-primary btn-large'
            ).action(lambda: comp.call(self.modal))

        return h.root

else:
    # if has_continuation and has_partial are False at the same time
    # code cannot run because we use an older Nagare version without stackless
    @presentation.render_for(App1)
    def render_app1(self, h, comp, *args):
        comp.on_answer(features.Partial(comp.becomes, self))
        h << h.a(
            'open modal',
            role='button',
            class_='btn btn-primary btn-large'
        ).action(features.Partial(comp.becomes, self.modal))

        return h.root


@presentation.render_for(App1, model='modal')
def render_modal(self, h, comp, *args):
    if not self.message:
        h << h.a('test ', self.counter).action(self.incr)
    else:
        h << h.span(self.message)
    return h.root


@presentation.render_for(App1, model='modal-footer')
def render_modal_footer(self, h, comp, *args):

    h << h.a('Close', class_='btn').action(comp.answer)
    if features.has_partial:
        h << h.a('Save changes', class_='btn btn-primary').action(
            features.Partial(self.commit, comp)
        )
    else:
        h << h.a('Save changes', class_='btn btn-primary').action(
            lambda: self.commit(comp)
        )

    return h.root
