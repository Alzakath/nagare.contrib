# -*- coding:utf-8 -*-
"""A nagare component used to display component in modal."""
from nagare import presentation, component


class Modal(object):

    """
    This a base class for modal modal.

    Attributes:
        comp: component wrapped by `Modal`
        model: model of the wrapped component
    """

    def __init__(self, comp, model, title=None):
        """Initialization

        Args:
            comp: component wrapped by the modal
            model: model of the wrapped component
        """
        self.comp = component.Component(comp, model=comp.model)
        self.body = component.Component(comp, model=model)
        self.footer = component.Component(comp, model=model + '-footer')
        self.title = title


@presentation.render_for(Modal)
def render(self, h, comp, *args):
    """
    Render html for Modal and opens it.

    Args:
        h: Nagare html renderer
        comp: Nagare Component to render
        *args: unused arguments needed for Nagare
    """
    id_ = h.generate_id('modal_')
    with h.div(class_='modal-wrap'):
        h << self.comp

        with h.div(class_='modal', id=id_):

            with h.div(class_='model-header'):
                h << h.a(u'\N{MULTIPLICATION X}',
                         class_='close').action(comp.answer)
                if self.title:
                    h << h.h3(self.title)

            self.body.on_answer(comp.answer)
            with h.div(class_='modal-body'):
                h << self.body.render(h.AsyncRenderer())

            self.footer.on_answer(comp.answer)
            with h.div(class_='modal-footer'):
                h << self.footer

        h << h.div(class_='modal-backdrop')

        # with h.script:
        h << h.script("""$(document).ready(function (){
            var modal = $('#%s');
            modal.modal({backdrop: '', keyboard: false});
        });""" % id_)

    return h.root
